from django.contrib import admin
from search.models import Thing

#automated slug creation
class ThingAdmin(admin.ModelAdmin):
	model = Thing
	list_display = ('name', 'description',)
	prepopulated_fields = {'slug': ('name',)}

# Register your models here.

admin.site.register(Thing, ThingAdmin)